import pytest
from selenium import webdriver

from pageobjects.board import BoardPage
from pageobjects.create_board import CreateBoardPage
from pageobjects.home import HomePage
from pageobjects.login import LoginPage


@pytest.fixture(scope="module")
def login_page(browser: webdriver) -> LoginPage:
    return LoginPage(browser)


@pytest.fixture(scope="module")
def home_page(browser: webdriver) -> HomePage:
    return HomePage(browser)


@pytest.fixture(scope="module")
def create_board_page(browser: webdriver) -> CreateBoardPage:
    return CreateBoardPage(browser)


@pytest.fixture(scope="module")
def board_page(browser: webdriver) -> BoardPage:
    return BoardPage(browser)
