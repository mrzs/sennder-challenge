import os

import pytest
from requests.structures import CaseInsensitiveDict
from selenium import webdriver


@pytest.fixture(scope="session")
def browser(request, base_url) -> webdriver:
    requested_driver = request.config.getoption("driver")
    browser: webdriver = init_webdriver(requested_driver)
    browser.base_url = base_url  # TODO think of a better way to set base_url
    yield browser
    browser.close()
    browser.quit()


def pytest_addoption(parser):
    group = parser.getgroup("selenium")
    group._addoption(
        "--driver",
        action="store",
        choices=SUPPORTED_DRIVERS,
        help="webdriver implementation.",
        metavar="str",
    )


SUPPORTED_DRIVERS = CaseInsensitiveDict(
    {
        "Chrome": webdriver.Chrome,
        "Firefox": webdriver.Firefox,
        "SauceLabs": webdriver.Remote,
    }
)


def init_webdriver(requested_driver) -> webdriver:
    if requested_driver == "SauceLabs":
        username = os.environ.get('SAUCELABS_USER')
        access_key = os.environ.get('SAUCELABS_KEY')
        executor = 'https://{}:{}@ondemand.eu-central-1.saucelabs.com:443/wd/hub'.format(username, access_key)
        desired_cap = {
            'platform': "Windows 10",
            'browserName': "chrome",
            'version': "latest",
        }
        return webdriver.Remote(
            command_executor=executor,
            desired_capabilities=desired_cap)
    else:
        return SUPPORTED_DRIVERS[requested_driver]()  # TODO add a proper driver init's
