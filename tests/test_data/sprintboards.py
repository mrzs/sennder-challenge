import os

from tests.utils import User


class SprintBoardsTestData:
    default_user_pass = os.environ.get('DEFAULT_USER_PASSWORD')
    DEFAULT_USER = User(name='Sennder',
                        email='sennderqa3@gmail.com',
                        password=default_user_pass)
