from collections import namedtuple


def format_locator(locator: tuple, substitute: str) -> tuple:
    return locator[0], locator[1].format(substitute)


# Named tuples used
Card = namedtuple('Card', ['title', 'description'])
User = namedtuple('User', ['name', 'email', 'password'])
