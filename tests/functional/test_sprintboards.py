from pageobjects.board import BoardPage
from pageobjects.create_board import CreateBoardPage
from pageobjects.home import HomePage
from pageobjects.login import LoginPage
from tests.test_data.sprintboards import SprintBoardsTestData as TD
from tests.utils import Card


# TODO:
#  * add 'application actions' layer of abstraction and rewrite the test method
#  * refactor for Chained method calls?
#  * add soft asserts
#  * allure reports
def test_add_cards(
        login_page: LoginPage,
        home_page: HomePage,
        create_board_page: CreateBoardPage,
        board_page: BoardPage
) -> None:
    login_page.open()
    login_page.do_login(TD.DEFAULT_USER.email, TD.DEFAULT_USER.password)
    home_page.create_board_link() \
        .click()

    assert create_board_page \
        .create_board_title() \
        .is_displayed()
    expected_url = create_board_page.get_url()
    actual_url = create_board_page.driver().current_url
    assert actual_url == expected_url

    create_board_page.do_create_board('My first board', TD.DEFAULT_USER.name)

    assert create_board_page \
        .created_modal_dialog_title() \
        .is_displayed()

    # Don't see any purpose in this check, so it's not implemented considering 'base_url'
    assert create_board_page.driver().current_url.startswith('https://sprintboards.io/boards')

    board_page.add_success_card_button().click()
    assert board_page \
        .add_card_dialog_header() \
        .is_displayed()

    went_well_card = Card("Goal was achieved", "Sprint was well planned")

    board_page.set_add_card_dialog_title(went_well_card.title)
    board_page.set_add_card_dialog_description(went_well_card.description)
    board_page.add_card_dialog_add_button().click()

    # Tries to find a card with specific title and verify description, so basically does two checks
    actual_card_description = board_page.get_card_description_by_title(went_well_card.title).text
    assert actual_card_description == went_well_card.description

    not_well_card = Card("Goal was not achieved", None)

    board_page.add_danger_card_button().click()
    assert board_page \
        .add_card_dialog_header() \
        .is_displayed()

    board_page.set_add_card_dialog_title(not_well_card.title)
    board_page.add_card_dialog_add_button().click()

    # Tries to find a card with specific title and verify description, so basically does two checks
    actual_card_description = board_page.get_card_description_by_title(not_well_card.title).text
    assert actual_card_description == "No description provided."

    went_well_likes = board_page.get_card_like_button_by_title(went_well_card.title)
    # No likes initially
    assert int(went_well_likes.text) == 0
    board_page.do_like_card_by_title(went_well_card.title)
    assert int(went_well_likes.text) == 1

    board_page.get_card_delete_button_by_title(not_well_card.title).click()

    assert board_page.delete_card_dialog_header().text == "Delete Card"
    assert board_page.delete_card_dialog_message().text == "Are you sure you want to continue?"

    board_page.delete_card_confirm_button().click()

    assert board_page.get_card_deleted_toast_message().text == "The card has been deleted"
    assert not board_page.card_exists_by_title(not_well_card.title)
