from abc import ABC, abstractmethod
from typing import Optional

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.remote import webelement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from pageobjects.map.urls import Url


class Page(ABC):

    @abstractmethod
    def driver(self) -> webdriver:
        pass

    @abstractmethod
    def open(self, url: Url = None) -> None:
        pass


class BasePage(Page):

    def __init__(self, browser: webdriver, url: Url) -> None:
        self._url: str = url.get(browser.base_url)
        self._driver: webdriver = browser

    def driver(self) -> webdriver:
        return self._driver

    def get_url(self) -> str:
        return self._url

    def open(self, url: Optional[Url] = None) -> None:
        if not url:
            url = self._url
        self._driver.get(url)

    def find_element(self, by: By, selector: str,
                     timeout=10) -> webelement:
        """find element wrapper with embedded wait functionality"""
        # TODO better wait management, webdriver wrapper for this(?), move default timeout to .ini
        element = WebDriverWait(self._driver, timeout).until(
            EC.presence_of_element_located((by, selector))
        )
        return element

    def element_exists(self, by: By, selector: str) -> bool:  # TODO webdriver wrapper for this(?)
        found = self._driver.find_elements(by, selector)
        return len(found) > 0
