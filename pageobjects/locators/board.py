from selenium.webdriver.common.by import By


class BoardPageLocators:

    ADD_SUCCESS_CARD_BUTTON = (By.CSS_SELECTOR, 'button.card.empty-card.text-success[type="button"]')
    ADD_DANGER_CARD_BUTTON = (By.CSS_SELECTOR, 'button.card.empty-card.text-danger[type="button"]')

    # Add Card Modal Dialog Locators
    ADD_CARD_DIALOG_HEADER = (By.CSS_SELECTOR, 'div#add-card-modal.modal-title.h4')
    ADD_CARD_DIALOG_TITLE_INPUT = (By.CSS_SELECTOR, 'input.form-control[placeholder="Required"]')
    ADD_CARD_DIALOG_DESCRIPTION_INPUT = (By.CSS_SELECTOR, 'textarea.form-control[placeholder="Optional"]')
    ADD_CARD_DIALOG_ADD_BUTTON = (By.XPATH, '//button[@type="submit" and text()="Add Card"]')

    # Card Related Locators
    CARD_BY_TITLE_DIV = (By.XPATH, '//div[div/h6[text()="{0}"]]')
    # Relative to specific card locators!
    CARD_DESCRIPTION_P = (By.CSS_SELECTOR, 'div.card-body p')
    CARD_LIKE_BUTTON = (By.XPATH, CARD_BY_TITLE_DIV[1]+'//button[*[@data-icon="thumbs-up"]]')
    CARD_DELETE_BUTTON = (By.XPATH, CARD_BY_TITLE_DIV[1]+'//button[*[@data-icon="times-circle"]]')

    # Delete Card Modal Dialog Locators
    DELETE_CARD_DIALOG_HEADER = (By.CSS_SELECTOR, 'div.modal-content > div.modal-header > div.modal-title.h4')
    DELETE_CARD_DIALOG_P = (By.CSS_SELECTOR, 'div.modal-content > div.modal-body > p')
    DELETE_CARD_CONFIRM_BUTTON = (By.XPATH, '//div[@class="modal-content"]/div/button[text()="Confirm"]')

    # Toast
    TOAST_CARD_DELETED_MESSAGE = (By.CSS_SELECTOR, 'div#toast-container > div.toast > div.toast-message')
