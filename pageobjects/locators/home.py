from selenium.webdriver.common.by import By


class HomePageLocators:
    CREATE_BOARD_LINK = (By.LINK_TEXT, 'Create Board')
