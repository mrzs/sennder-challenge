from selenium.webdriver.common.by import By


class CreateBoardPageLocators:

    OWNER_SELECT = (By.XPATH, '//select[option[text()="Choose Owner..."]]')
    OWNER_BY_NAME_OPTION = (By.XPATH, '//option[text()="{0}"]')
    CREATE_BOARD_BUTTON = (By.XPATH, '//button[@type="submit" and text()="Create Board"]')
    SESSION_NAME_INPUT = (By.CSS_SELECTOR, 'input[placeholder="Session Name"]')
    CREATE_BOARD_TITLE = (By.LINK_TEXT, 'Create Board')
    CREATED_MODAL_DIALOG_TITLE = (By.XPATH, '//div[@class="swal-modal" and @role="dialog"]/div[text()="Created"]')

