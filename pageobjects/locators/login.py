from selenium.webdriver.common.by import By


class LoginPageLocators:
    EMAIL_INPUT = (By.CSS_SELECTOR, 'input[type=email]')
    PASSWORD_INPUT = (By.CSS_SELECTOR, 'input[type=password]')
    LOGIN_BUTTON = (By.CSS_SELECTOR, 'button[type=submit]')
