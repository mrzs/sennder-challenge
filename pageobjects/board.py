from selenium import webdriver
from selenium.webdriver.remote import webelement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from pageobjects.base import BasePage
from pageobjects.locators.board import BoardPageLocators as Lc
from pageobjects.map.urls import BoardPageUrl
from tests.utils import format_locator


class BoardPage(BasePage):

    def __init__(self, browser: webdriver) -> None:
        super(BoardPage, self).__init__(browser, BoardPageUrl(self.get_board_id()))

    def get_board_id(self) -> str:
        pass  # TODO

    def add_success_card_button(self) -> webelement:
        return self.find_element(*Lc.ADD_SUCCESS_CARD_BUTTON)

    def add_danger_card_button(self) -> webelement:
        return self.find_element(*Lc.ADD_DANGER_CARD_BUTTON)

    # Card Related methods TODO refactor working with cards logic
    def get_card_by_title(self, title: str) -> webelement:
        return self.find_element(*format_locator(Lc.CARD_BY_TITLE_DIV, title))

    def card_exists_by_title(self, title: str) -> bool:
        return self.element_exists(*format_locator(Lc.CARD_BY_TITLE_DIV, title))

    def get_card_description_by_title(self, title: str) -> webelement:
        return self.get_card_by_title(title) \
            .find_element(*Lc.CARD_DESCRIPTION_P)

    def get_card_like_button_by_title(self, title: str) -> webelement:
        return self.find_element(*format_locator(Lc.CARD_LIKE_BUTTON, title))

    def do_like_card_by_title(self, title: str) -> None:  # TODO move to actions layer
        like_button = self.get_card_like_button_by_title(title)
        expected_likes = str(int(like_button.text) + 1)  # hack here =(
        like_button.click()
        WebDriverWait(self.driver(), 5).until(  # TODO proper wait management please?
            EC.text_to_be_present_in_element(format_locator(Lc.CARD_LIKE_BUTTON, title), expected_likes)
        )

    def get_card_delete_button_by_title(self, title: str) -> webelement:
        return self.find_element(*format_locator(Lc.CARD_DELETE_BUTTON, title))

    # Add Card Modal Dialog methods TODO move this modal dialog to separate entity
    def add_card_dialog_header(self) -> webelement:
        header = self.find_element(*Lc.ADD_CARD_DIALOG_HEADER)
        WebDriverWait(self._driver, 5).until(  # TODO proper wait management please?
            EC.visibility_of(header)
        )
        return header

    def set_add_card_dialog_title(self, title: str) -> None:
        self.find_element(*Lc.ADD_CARD_DIALOG_TITLE_INPUT) \
            .send_keys(title)

    def set_add_card_dialog_description(self, description: str) -> None:
        self.find_element(*Lc.ADD_CARD_DIALOG_DESCRIPTION_INPUT) \
            .send_keys(description)

    def add_card_dialog_add_button(self) -> webelement:
        return self.find_element(*Lc.ADD_CARD_DIALOG_ADD_BUTTON)

    # Delete Card Modal Dialog methods TODO move this modal dialog to separate entity
    def delete_card_dialog_header(self) -> webelement:
        return self.find_element(*Lc.DELETE_CARD_DIALOG_HEADER)

    def delete_card_dialog_message(self) -> webelement:
        return self.find_element(*Lc.DELETE_CARD_DIALOG_P)

    def delete_card_confirm_button(self) -> webelement:
        return self.find_element(*Lc.DELETE_CARD_CONFIRM_BUTTON)

    # Toast message
    def get_card_deleted_toast_message(self) -> webelement:
        return self.find_element(*Lc.TOAST_CARD_DELETED_MESSAGE)
