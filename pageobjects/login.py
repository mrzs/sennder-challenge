from selenium import webdriver
from selenium.webdriver.remote import webelement

from pageobjects.base import BasePage
from pageobjects.locators.login import LoginPageLocators as Lc
from pageobjects.map.urls import LoginPageUrl


class LoginPage(BasePage):

    def __init__(self, browser: webdriver) -> None:
        super(LoginPage, self).__init__(browser, LoginPageUrl())

    def set_email(self, email: str) -> None:
        self.find_element(*Lc.EMAIL_INPUT) \
            .send_keys(email)

    def set_password(self, password: str) -> None:
        self.find_element(*Lc.PASSWORD_INPUT) \
            .send_keys(password)

    def login_button(self) -> webelement:
        return self.find_element(*Lc.LOGIN_BUTTON)

    def do_login(self, email: str, password: str) -> None:  # TODO move to application actions layer
        self.set_email(email)
        self.set_password(password)
        self.login_button() \
            .click()
