import urllib
from abc import ABC, abstractmethod


class Url(ABC):

    @abstractmethod
    def get(self, base_url: str) -> str:
        pass


class HomePageUrl(Url):

    def __init__(self) -> None:
        self._page_path: str = ''

    def get(self, base_url: str) -> str:
        return urllib.parse.urljoin(base_url, self._page_path)


class LoginPageUrl(HomePageUrl):

    def __init__(self) -> None:
        self._page_path: str = '/auth/login'


class CreateBoardPageUrl(HomePageUrl):

    def __init__(self) -> None:
        self._page_path: str = '/boards/create'


class BoardPageUrl(HomePageUrl):

    def __init__(self, board_id: str = '') -> None:
        self._page_path: str = '/boards/{0}'.format(board_id)
