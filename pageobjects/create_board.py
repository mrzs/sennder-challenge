from selenium import webdriver
from selenium.webdriver.remote import webelement

from pageobjects.base import BasePage
from pageobjects.locators.create_board import CreateBoardPageLocators as Lc
from pageobjects.map.urls import CreateBoardPageUrl
from tests.utils import format_locator


class CreateBoardPage(BasePage):

    def __init__(self, browser: webdriver) -> None:
        super(CreateBoardPage, self).__init__(browser, CreateBoardPageUrl())

    def create_board_title(self) -> webelement:
        return self.find_element(*Lc.CREATE_BOARD_TITLE)

    def create_board_button(self) -> webelement:
        return self.find_element(*Lc.CREATE_BOARD_BUTTON)

    def created_modal_dialog_title(self) -> webelement:
        return self.find_element(*Lc.CREATED_MODAL_DIALOG_TITLE)

    def set_session_name(self, session_name: str) -> None:
        self.find_element(*Lc.SESSION_NAME_INPUT) \
            .send_keys(session_name)

    def set_owner(self, owner: str) -> None:  # TODO move select dropdown to utils
        self.find_element(*Lc.OWNER_SELECT) \
            .find_element(*format_locator(Lc.OWNER_BY_NAME_OPTION, owner))  \
            .click()

    def do_create_board(self, session_name: str, owner: str) -> None:
        # TODO add other options, move to application actions layer
        self.set_session_name(session_name)
        self.set_owner(owner)
        self.create_board_button() \
            .click()
