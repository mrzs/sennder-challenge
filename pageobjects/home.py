from selenium import webdriver
from selenium.webdriver.remote import webelement

from pageobjects.base import BasePage
from pageobjects.locators.home import HomePageLocators as Lc
from pageobjects.map.urls import HomePageUrl


class HomePage(BasePage):

    def __init__(self, browser: webdriver) -> None:
        super(HomePage, self).__init__(browser, HomePageUrl())

    def create_board_link(self) -> webelement:
        return self.find_element(*Lc.CREATE_BOARD_LINK)
